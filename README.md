# antGenus2sppRatios



## How many genera should there be, given a certain number of species? 

As of February of 2024, I think there are about 100 species of ants in Palau, and about 52 genera. This may be more genera than there are in Fiji, which is a much larger area, and has about twice as many species. But is this significanty more genera than you'd expect? In an assemblage where competitive exclusion is an important structuring process, one might expect taxa to be more distantly related than one would expect by chance draws from the surrounding species pool. 

One easy way to test this with Palau ants is by taking fauna lists of nearby areas, and just sampling 100 random species from their lists, and counting how many unique genera you get (actually, you can just crop off the specific epithets, since you just need to know how many times different genera are represented in the species pool, not what the species identities are). This is what I've started to do here, using lists from AntWeb.org and a recent publication on the ants of Singapore (see overview here: https://antwiki.org/wiki/Singapore ).

In the "genusLists" directory, I've placed text files (each with the heading "genus") in which I've taken species lists, collapsed the subspecies, and then just listed each genus the number of times that matches the number of species. For example, if there are two Camponotus species and three Pheidole species present in an assemblage, the text would just list: 
genus
Camponotus
Camponotus
Pheidole
Pheidole
Pheidole

Then, if Palau only had two species, I could randomly select two words from that list 100 times, and get the average number of unique genera that you'd get from a random draw of two species from that species pool. 

In the future, I might try to do something a bit more fancy using GBIF data, and only sampling specimen observations that occur, e.g., within 10km of the coast, which might be a more realistic "species pool" when thinking about the way dispersal is likely to work in the region. But for now, the taxon lists are a quick and dirty way of exploring this crude phylogenetic community assembly pattern. 

I might also try this with non-ant taxa at some point...
